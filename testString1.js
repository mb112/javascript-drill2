const stringToNumber = require('./string1')

console.log('Test Case 1 = ',stringToNumber('$4567'))
console.log('Test Case 2 = ',stringToNumber('-$4567'))
console.log('Test Case 3 =',stringToNumber("$1,002.22"))
console.log('Test Case 4 =',stringToNumber('-$4567.57'))
console.log('Test Case 5 =',stringToNumber('w3445'))
console.log('Test Case 6 =',stringToNumber(null))
