const ipSplit = require('./string2')


console.log('Test Case 1 = ', ipSplit('192.0. 2.146'))
console.log('Test Case 2 = ', ipSplit('2001:0db8:85a3:0000:0000:8a2e:0370:7334'))
console.log('Test Case 3 = ', ipSplit(null))
console.log('Test Case 4 = ', ipSplit('192.0.256.146'))
console.log('Test Case 5 = ', ipSplit(''))
