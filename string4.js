function titleCase(str) {
    str = str.toLowerCase().split(' ')
    for (let i = 0; i < str.length; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].slice(1)
    }
    return str.join(' ')
}


function fullName(obj) {
    if ((obj === null) || (Object.keys(obj).length === 0)) {
        return 'Invalid object'
    }
    else {
        let x = []
        if (obj.first_name){
            x.push(obj.first_name)
        }if (obj.middle_name){
            x.push(obj.middle_name)
        }if (obj.last_name){
            x.push(obj.last_name)
        }
        let name = titleCase(x.join(' '))
        return name
    }
}

module.exports = fullName


