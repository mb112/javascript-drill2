function monthOfDate(string) {
    if (string) {
        let dateArray = string.split('/')
        return dateArray[1]
    }
    return 'Invalid date'
}

module.exports = monthOfDate