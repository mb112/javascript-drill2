function verifyIpFormat(IP) {
    let x = IP.split('.')
    if (x.length === 4) {
        for (let i = 0; i < x.length; i++) {
            if (x[i] > 255) {
                return false
            }
        }
        return true
    } else {
        return false
    }
}


function ipSplit(IPV4) {
    if ((IPV4)) {
        if (verifyIpFormat(IPV4)){
            return IPV4.split('.')
        }
    }
    return []
}


module.exports = ipSplit

