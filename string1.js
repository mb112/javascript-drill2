function stringToNumber(string) {
    if (string) {
        let x = parseFloat((string.replace('$', '').replace(',','')))
        if (isNaN(x)) {
            return 0
        } else {
            return x
        }
    }
    return 0
}

module.exports = stringToNumber




