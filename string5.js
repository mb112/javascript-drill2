function arrayToSentence(array){
    if (array === null) {
        return 'Invalid Input'
    }
    else if (array === []){
        return ''
    }
    else {
    return array.join(' ')}
}

module.exports = arrayToSentence