const fullName = require('./string4')

console.log('Test Case 1 =',fullName({"first_name": "JoHN", "last_name": "SMith"}))
console.log('Test Case 2 =',fullName({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}))
console.log('Test Case 3 =',fullName({ last_name: "Beckham", first_name: "David"}))
console.log('Test Case 4 =',fullName(null))
console.log('Test Case 5 =',fullName({}))

